import * as constants from '../constants'

// SETTINGS
export const setLanguage = language => ({type: constants.SET_LANGUAGE, language})
export const setTheme = theme => ({type: constants.SET_THEME, theme})


// GAME
export const setSize = size => {
  return (dispatch) => {
  
    dispatch({
      type: constants.SET_SIZE, size
    })
    
    dispatch({
      type: constants.INIT_BOARD_VALUES
    })
  }
}

export const initBoardValues = () => ({type: constants.INIT_BOARD_VALUES})
export const setOverturningDelay = delay => ({type: constants.SET_OVERTURNING_DELAY, delay})


function calcResult(card1, card2, target) {
  if (!card1.operator && card2.operator) {
    switch (card2.operator) {
      case '-':
        return card1.value - card2.value === target.value
      case '+':
        return card1.value + card2.value === target.value
      case '*':
        return card1.value * card2.value === target.value
      case '/':
        return card1.value / card2.value === target.value
      default:
        return false
    }
  }
  else return false
}

export const setCardOverturned = (id, overturn) =>{
  return (dispatch, getState) => {
  
    let state = getState()
    
    if(state.game.openCards.length === 2) {
      dispatch({type: constants.UNOVERTURNED_ALL_CARDS})
      
      if (state.game.openCards.find(opendId => id === opendId) && !overturn) return;
    }
    dispatch({type: constants.SET_CARD_OVERTURNED, id, overturn})
  
    /////// CHECK RESULT OF 2 CARDS
    
    state = getState()
    
    if (state.game.openCards.length === 2) {
      let card1 = state.game.calculatedValues[state.game.openCards[0]]
      let card2 = state.game.calculatedValues[state.game.openCards[1]]
      let target = state.game.target
      const isSuccess = calcResult(card1, card2, target)
      if (isSuccess) {
        dispatch({type: constants.SET_NEXT_TARGET})
        dispatch({type: constants.EXCLUDE_CARDS})
      }
    }
  }
}

export const setMinTarget = min => {
  return (dispatch) => {
    
    dispatch({
      type: constants.SET_MIN_TARGET, min
    })
    
    dispatch({
      type: constants.INIT_BOARD_VALUES
    })
  }
}


export const setMaxTarget = max => {
  return (dispatch) => {
    
    dispatch({
      type: constants.SET_MAX_TARGET, max
    })
    
    dispatch({
      type: constants.INIT_BOARD_VALUES
    })
  }
}



export const toggleRandom = random => {
  return (dispatch) => {
    
    dispatch({
      type: constants.TOGGLE_RANDOM, random
    })
    
    dispatch({
      type: constants.INIT_BOARD_VALUES
    })
  }
}


import React, { Component } from 'react';
// import { BrowserRouter, Route, Link } from 'react-router-dom'
import Board from '../components/Board';
import GameSettings from '../components/GameSettings';


class App extends Component {
  render() {
    return (
      <div>
        <GameSettings />
        <Board />
      </div>
    );
  }
}

export default App;

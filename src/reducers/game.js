import rn from 'random-number'
import {
  DIVIDE,
  EXCLUDE_CARDS,
  INIT_BOARD_VALUES,
  MINUS,
  MULTIPLY,
  OPERATORS,
  PLUS,
  SET_CARD_OVERTURNED,
  SET_MAX_TARGET,
  SET_MIN_TARGET,
  SET_NEXT_TARGET,
  SET_OVERTURNING_DELAY,
  SET_SIZE,
  TOGGLE_RANDOM,
  UNOVERTURNED_ALL_CARDS
} from '../constants';
import { arrToMap, getRandomArrayElement, ID, shuffleArray } from '../utils'


let defaultState = {
  delay: 3000,
  size: {
    width: 4,
    height: 4
  },
  min: 1,
  max: 20,
  random: true,
  maxCalculated: 100,
  minCalculated: 100,
  targetValues: [],
  target: {},
  calculatedValues: {},
  openCards: []
};

let targetIterator = 0;

export default (state = defaultState, action) => {
  const { type, size, delay, min, max, random, id, overturn } = action;
  
  switch (type) {
    case SET_SIZE:
      return {...state, size: {...state.size, ...size}};
      
    case SET_OVERTURNING_DELAY:
      return {
        ...state,
        delay
      };
      
    case SET_MIN_TARGET:
      return {
        ...state,
        min
      };
      
    case SET_MAX_TARGET:
      return {
        ...state,
        max
      };
      
    case TOGGLE_RANDOM:
      return {
        ...state,
        random
      };
      
    case UNOVERTURNED_ALL_CARDS:
      return {
        ...state,
        openCards: [],
        calculatedValues: {
          ...state.calculatedValues,
          
          [state.openCards[0]]: {
            ...state.calculatedValues[state.openCards[0]],
            overturned: false
          },
          
          [state.openCards[1]]: {
            ...state.calculatedValues[state.openCards[1]],
            overturned: false
          },
        }
      };
      
    case SET_CARD_OVERTURNED:
      let openCards = []
      if (!overturn) {
        openCards = state.openCards.filter(openedId => id !== openedId)
      }
      else {
        openCards = [...state.openCards, id]
      }
      return {
        ...state,
        openCards,
        calculatedValues: {...state.calculatedValues, [id]: {...state.calculatedValues[id], overturned: overturn}}
      };
  
    case EXCLUDE_CARDS:
      return {
        ...state,
        openCards: [],
        calculatedValues: {
          ...state.calculatedValues,
    
          [state.openCards[0]]: {
            ...state.calculatedValues[state.openCards[0]],
            excluded: true
          },
    
          [state.openCards[1]]: {
            ...state.calculatedValues[state.openCards[1]],
            excluded: true
          },
        }
      }
  
    case SET_NEXT_TARGET:
      targetIterator++
      return {
        ...state,
        target: state.targetValues[targetIterator]
      }
      
    case INIT_BOARD_VALUES:
      const options = {
        min:  state.min,
        max:  state.max,
        integer: true
      }
  
      targetIterator = 0;
      
      const rnGenerator = rn.generator(options)
      const cardsLength = state.size.width * state.size.height
      const moduloTargetLength = cardsLength % 2
      let targetLength = Math.floor((cardsLength - moduloTargetLength) / 2)
      
      let targetValues = []
      let calculatedValues = []
      
      do {
        const operator = getRandomArrayElement(OPERATORS);
        let targetValue = rnGenerator()
        
        while(targetValue === 0) {
          targetValue = rnGenerator()
        }
        targetValues = [...targetValues, {operator, value: targetValue, id: ID()}]
        
        
        let generatedCalcNumber = rnGenerator()
        while(generatedCalcNumber === 0) {
          generatedCalcNumber = rnGenerator()
        }
  
        let calculatedNumber, operatorForGenerated;
        
        switch (operator) {
          case MINUS:
            calculatedNumber = targetValue + generatedCalcNumber
            operatorForGenerated = MINUS
            break;
          case PLUS:
            calculatedNumber = targetValue - generatedCalcNumber
            operatorForGenerated = PLUS
            break;
          case DIVIDE:
            calculatedNumber = targetValue * generatedCalcNumber
            
            let counterOfTry = 0
            while((calculatedNumber > state.maxCalculated || calculatedNumber < state.minCalculated) && counterOfTry < 20) {
              generatedCalcNumber = rnGenerator()
              calculatedNumber = targetValue * generatedCalcNumber
              counterOfTry++
            }
            
            if (calculatedNumber > state.maxCalculated || calculatedNumber < state.minCalculated) {
              generatedCalcNumber = rnGenerator(1, 10, true)
              calculatedNumber = targetValue * generatedCalcNumber
            }
            operatorForGenerated = DIVIDE
            break;
          case MULTIPLY:
            while(generatedCalcNumber === 0 || targetValue < generatedCalcNumber || targetValue % generatedCalcNumber !== 0) {
              generatedCalcNumber = rnGenerator()
            }
            calculatedNumber = Math.floor(targetValue / generatedCalcNumber)
            operatorForGenerated = MULTIPLY
            break;
          default:
            throw new Error('No have any operators')
        }
  
        calculatedValues = [...calculatedValues,
          {value: calculatedNumber,
            overturned: false,
            id: ID()
          },
          {
            operator: operatorForGenerated,
            value: generatedCalcNumber,
            target: targetValue,
            targetOperator: operator,
            overturned: false,
            id: ID()
          },
        ]
        
        targetLength--
  
  
        function test() {
          switch (operatorForGenerated) {
            case '-':
              return calculatedNumber - generatedCalcNumber === targetValue
            case '+':
              return calculatedNumber + generatedCalcNumber === targetValue
            case '*':
              return calculatedNumber * generatedCalcNumber === targetValue
            case '/':
              return calculatedNumber / generatedCalcNumber === targetValue
            default:
              return false
          }
        }
        
        if (!test()) {
          debugger;
          console.log('Invalid calculation: x, operator, y, result: ', calculatedNumber, operatorForGenerated, generatedCalcNumber, targetValue)
        }
        
      }
      while (targetLength > 0)
  
      
  
      if (state.random) {
        targetValues = shuffleArray(targetValues)
        calculatedValues = shuffleArray(calculatedValues)
      }
      
      return {
        ...state,
        openCards: [],
        targetValues: targetValues,
        target: targetValues[targetIterator],
        calculatedValues: arrToMap(calculatedValues)
      };
      
    default:
      return state
  }
}

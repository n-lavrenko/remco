import { LIGHT, SET_LANGUAGE, SET_THEME } from '../constants';


let defaultState = {
  language: 'en',
  theme: LIGHT
};

export default (state = defaultState, action) => {
  const { type, payload } = action;
  
  switch (type) {
    case SET_LANGUAGE:
      const { language } = payload;
      return {
        ...state,
        language
      };
    case SET_THEME:
      const { theme } = payload;
      return {
        ...state,
        theme
      };
    default:
      return state
  }
  
}

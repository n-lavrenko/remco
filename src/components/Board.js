import React, { Component } from 'react';
import { connect } from 'react-redux';
import { initBoardValues, setCardOverturned } from '../AC';

import { objectToArray } from '../utils';
import Card from './Card';
import TargetValues from './TargetValues';


class Board extends Component {
  
  componentWillMount() {
    this.props.initBoardValues()
  }
  
  render() {
    const { game: { size: { width, height }, targetValues, calculatedValues, target }, setCardOverturned } = this.props
    const {game} = this.props
    
    
    if (!targetValues.length) {
      return null;
    }
  
    let arrayCalculatedValues = objectToArray(calculatedValues)
    
    let indexValue = -1
    function getNextVal() {
      indexValue++
      return arrayCalculatedValues[indexValue]
    }
    
    let rows = []
    
    for (let h = 0; h < height; h++) {
      rows.push(<div className='row' key={ h }>
        { getCards() }
      </div>)
    }
    
    function getCards() {
      let cards = []
      for (let w = 0; w < width; w++) {
        let data = getNextVal()
        if (!data) return null
        cards.push(
          <Card data={ data } key={ data.id } game={game} action={ setCardOverturned }/>
        )
      }
      return cards;
    }
    
    return (
      <div className='board'>
        <TargetValues targets={ targetValues } target={ target } />
        { rows }
      </div>
    );
  }
}

function mapStateToProps({ game }) {
  return {
    game
  };
}

export default connect(
  mapStateToProps, { initBoardValues, setCardOverturned }
)(Board);

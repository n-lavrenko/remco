import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setMaxTarget, setMinTarget, setOverturningDelay, setSize, toggleRandom } from '../AC';


class GameSettings extends Component {
  
  state = {
    showSettings: false
  }
  
  render() {
    const { size: {width, height}, delay, min, max, random } = this.props.game
    const {showSettings} = this.state
    
    return (
      <div>
        <div className='game-settings' style={ {display: showSettings ? 'flex': 'none'} }>
          <div className='panel'>
            <div>
              Width: {width} <br/>
              <input type="range" value={ width } min='2' max='15' onChange={ this.handleSizeChange('width') } />
            </div>
      
            <div>
              Height: {height}<br/>
              <input type="range" value={ height } min='2' max='15' onChange={ this.handleSizeChange('height') } />
            </div>
            
            <div>
              Delay: {delay} <br/>
              <input type="range" value={ delay } min='200' max='10000' onChange={ this.handleDelayChange } />
            </div>
            
            <div>
              Min Target Value: {min} <br/>
              <input type="range" value={ min } min='-100' max={ max -1 } onChange={ this.handleMinChange } />
            </div>
            
            <div>
              Max Target Value: {max} <br/>
              <input type="range" value={ max } min={min + 1} max='100' onChange={ this.handleMaxChange } />
            </div>
          </div>
          <div className='panel'>
            <div>
              <label>
                Randomize
                <input type='checkbox' checked={ random } onChange={ this.handleRandomChange } />
              </label>
            </div>
          </div>
        </div>
        <div className="center">
          <button onClick={() => this.toggleSettings()}>{showSettings ? 'Hide' : 'Show'} settings</button>
        </div>
      </div>
    );
  }
  
  toggleSettings() {
    this.setState({showSettings: !this.state.showSettings})
  }
  
  handleSizeChange = type => ev => {
    const { value } = ev.target;
    
    const newSize = {[type]: +value}
    this.props.setSize(newSize)
    
  }
  
  handleDelayChange = ev => {
    const { value } = ev.target;
    this.props.setOverturningDelay(value)
  }
  
  handleMinChange = ev => {
    const { value } = ev.target;
    this.props.setMinTarget(+value)
  }
  
  handleMaxChange = ev => {
    const { value } = ev.target;
    this.props.setMaxTarget(+value)
  }
  
  handleRandomChange = ev => {
    const { checked } = ev.target;
    this.props.toggleRandom(checked)
  }
}

function mapStateToProps(state) {
  return {
    game: state.game
  };
}

export default connect(
  mapStateToProps, { setSize, setOverturningDelay, setMinTarget, setMaxTarget, toggleRandom }
)(GameSettings);

import React, { Component } from 'react';


class Card extends Component {
  
  componentWillReceiveProps(nextProps) {
    // console.log('---', nextProps)
  }
  
  render() {
    let { id, value, operator, target, targetOperator, overturned, excluded } = this.props.data
    const { random } = this.props.game
    
    if (value < 0) {
      value = `(${value})`
    }
    
    return (
      <div className={"card " + (overturned ? 'flip ': " ") + (excluded ? 'excluded ': " ")}>
        <div>
          <div className="front" onClick={() => this.onCardClick(true, id)}> ? </div>
          <div className="back" onClick={() => this.onCardClick(false, id)}>
            {!random && target && <div className='card-target'><i>{targetOperator} {target}</i></div>}
            {operator ? <span>{operator} {` `} { value }</span>: value}
          </div>
        </div>
      </div>
    );
  }
  
  onCardClick(overturn, id) {
    const { action, data } = this.props;
    if (data.excluded) return;
    action(id, overturn)
  }
  
}

export default Card;

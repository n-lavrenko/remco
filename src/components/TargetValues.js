import React, { Component } from 'react';


class TargetValues extends Component {
  
  state = {
    showAllTargets: false
  }
  
  render() {
    const { target } = this.props
    const { showAllTargets } = this.state
    
    if (!target) {
      return <h1>You Win!</h1>
    }
    
    return (
      <div className='target-values'>
        <div className="center m-b-20">
          <button onClick={() => this.toggleShowAllTargets()}>{showAllTargets ? 'Hide' : 'Show'} all targets</button>
        </div>
  
        { this.getAllTargets() }
        
        <div className="target">
          <div className='operator'>
            <h2>Operator</h2>
            <h1>{ target.operator }</h1>
            </div>
          <div className='value'>
            <h2>Result</h2>
            <h1>{ target.value }</h1>
          </div>
        </div>
      </div>
    );
  }
  
  toggleShowAllTargets() {
    this.setState({showAllTargets: !this.state.showAllTargets})
  }
  
  getAllTargets = () => {
    const { targets } = this.props
    if (!this.state.showAllTargets) return null
    return (
      <div>
        <h4>Targets values:</h4>
        { targets.map((value, key) =>
          <div className='target-value-item' key={ key }># { key + 1 }: Operator: { value.operator },
            value: { ` ` } { value.value }</div>) }
      </div>
    )
  }
}

export default TargetValues

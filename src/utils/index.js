export const getRandomArrayElement = (arr) => arr[Math.floor(Math.random() * arr.length)]

export const shuffleArray = arr => arr.sort(() => Math.random() - 0.5);
export const ID = () => '_' + Math.random().toString(36).substr(2, 15)

export function arrToMap(arr) {
  return arr.reduce((acc, item) => ({
    ...acc,
    [item.id]: item
  }), {})
}


export function objectToArray(object) {
  return Object.values(object)
}

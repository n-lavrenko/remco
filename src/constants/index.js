export const INIT_BOARD_VALUES = 'INIT_BOARD_VALUES';
export const SET_SIZE = 'SET_SIZE';
export const SET_LANGUAGE = 'SET_LANGUAGE';
export const SET_OVERTURNING_DELAY = 'SET_OVERTURNING_DELAY';
export const SET_MIN_TARGET = 'SET_MIN_TARGET';
export const SET_MAX_TARGET = 'SET_MAX_TARGET';
export const TOGGLE_RANDOM = 'TOGGLE_RANDOM';
export const SET_CARD_OVERTURNED = 'SET_CARD_OVERTURNED';
export const UNOVERTURNED_ALL_CARDS = 'UNOVERTURNED_ALL_CARDS';
export const SET_NEXT_TARGET = 'SET_NEXT_TARGET';
export const EXCLUDE_CARDS = 'EXCLUDE_CARDS';


export const SET_THEME = 'SET_THEME';
export const DARK = 'DARK';
export const LIGHT = 'LIGHT';

export const MULTIPLY = '*';
export const DIVIDE = '/';
export const PLUS = '+';
export const MINUS = '-';
export const OPERATORS = [MULTIPLY, DIVIDE, MINUS, PLUS ];

